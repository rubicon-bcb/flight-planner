package com.rubiconproject.flightplanner;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Immutable domain object representing an airport
 *
 * @see {@code ports.csv}
 * @author erachitskiy
 */
public class Airport {
    private final String code;
    private final double latitude;
    private final double longitude;

    /**
     * Construct an airport given a code, a latitude and a longitude
     * @param code airport code
     * @param latitude airport latitude
     * @param longitude airport longitude
     */
    public Airport(String code, double latitude, double longitude) {
        this.code = code;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     * Get three letter FAA airport code
     * @return three letter FAA airport code
     */
    public String getCode() {
        return code;
    }

    /**
     * Get airport latitude in decimal degrees
     * @return airport latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Get airport longitude in decimal degrees
     * @return airport longitude
     */
    public double getLongitude() {
        return longitude;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
