package com.rubiconproject.flightplanner;


import java.util.*;

/**
 * A flight planner class used to find routes between airports
 *
 * @author erachitskiy
 */
public class FlightPlanner {
    private final FlightDatabase flightDatabase = new FlightDatabase();

    /**
     * Find the cheapest possible route (see {@link com.rubiconproject.flightplanner.Flight#getCost()}) between two airports
     * returning {@code null} if a route could not be found
     *
     * @param src a departure airport FAA code (e.g. LAX)
     * @param dst a destination airport FAA code (e.g. EWR)
     * @return a list of flights representing the cheapest route between the two airports
     */
    public List<Flight> findRoute(String src,final String dst){
        /*
            TODO: implement me
         */
        return null;
    }
}
